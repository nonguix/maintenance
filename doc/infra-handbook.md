# Infrastructure handbook for nonguix

## Update Postgresql DB for cuirass
Make a backup:
```
sudo herd stop cuirass && sudo herd disable cuirass
sudo -u cuirass -s /bin/sh -c 'pg_dump --username=cuirass cuirass > /var/lib/cuirass/cuirass_dump.sql'
sudo mv /var/lib/cuirass/cuirass_dump.sql /data
```
Download Postgres binaries for target version
```
guix build postgresql@13
```
Upgrade the database:
```
sudo herd stop postgres
sudo mv /var/lib/postgresql/data{,.pg10}
sudo mkdir /var/lib/postgresql/data && sudo chown postgres:postgres /var/lib/postgresql/data
sudo su - postgres -s /bin/sh -c "cd /var/lib/postgresql/data; /gnu/store/m5d3d0f8lnrmj2glf0rdid72ybvwbxdd-postgresql-13.16/bin/initdb -D /var/lib/postgresql/data --locale=en_US.UTF-8"
sudo su - postgres -s /bin/sh -c "cd /var/lib/postgresql/data; /gnu/store/m5d3d0f8lnrmj2glf0rdid72ybvwbxdd-postgresql-13.16/bin/pg_upgrade --old-datadir=/var/lib/postgresql/data.pg10 --new-datadir=/var/lib/postgresql/data --old-bindir=/gnu/store/n3r7rvnwn1q6gm6wy0kcpzk92jcwcjic-postgresql-10.23/bin --new-bindir=/gnu/store/m5d3d0f8lnrmj2glf0rdid72ybvwbxdd-postgresql-13.16/bin"
```
Reconfigure with the target PostgreSQL version configured
```scheme
(service postgresql-service-type
  (postgresql-configuration
    (postgresql postgresql-13)))
```
Enable and start the cuirass services:
```
sudo herd enable cuirass
sudo herd start cuirass
sudo herd start cuirass-remote-server
sudo herd start cuirass-web
```
