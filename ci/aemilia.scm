;; This is the configuration for nonguix build server.

(use-modules (gnu)
             (gnu packages databases)
             (gnu packages virtualization)
             (guix modules)
             (ice-9 match))
(use-service-modules admin
                     avahi
                     certbot
                     cuirass
                     databases
                     mcron
                     networking
                     shepherd
                     ssh
                     web)

(define %cuirass-specs
  #~(list
     (specification
      (name "nonguix")
      (build '(channels nonguix))
      (channels
       (list %default-guix-channel
             (channel
              (name 'nonguix)
              (url "https://gitlab.com/nonguix/nonguix.git")
              (introduction
                (make-channel-introduction
                  "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
                  (openpgp-fingerprint
                    "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))))
      (priority 0)
      (systems '("x86_64-linux")))))

(define %nginx-deploy-hook
  (program-file
   "nginx-deploy-hook"
   #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
       (kill pid SIGHUP))))

;; Run the garbe collector every day at 4:00 AM
(define garbage-collector-job
  #~(job "0 4 * * *"
         "guix gc"))

;; CIFS mount disappears often
(define mount-all-job
  #~(job "0 * * * *"
         "mount --all"
         #:user "root"))

(define-public default-module-filter
  (match-lambda
    (('guix 'config) #f)
    (('guix _ ...) #t)
    (('gnu _ ...) #t)
    (('nongnu _ ...) #t)
    (('nonguix _ ...) #t)
    (_ #f)))

(define-syntax-rule (with-service-gexp-modules body ...)
  (with-imported-modules (source-module-closure
                          (append '((gnu build shepherd))
                                  ;; This %default-modules is from (gnu services shepherd).
                                  %default-modules)
                          #:select? default-module-filter)
    body ...))

(operating-system
  (locale "de_DE.utf8")
  (timezone "Europe/Berlin")
  (keyboard-layout (keyboard-layout "de"))
  (host-name "aemilia")
  (users (cons* (user-account
                  (name "jonathan")
                  (comment "Jonathan Brielmaier")
                  (group "users")
                  (home-directory "/home/jonathan")
                  (supplementary-groups
                   '("wheel" "netdev" "kvm")))
                (user-account
                  (name "podiki")
                  (group "users")
                  (home-directory "/home/podiki")
                  (supplementary-groups
                    '("wheel" "netdev" "kvm")))
                %base-user-accounts))
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (targets '("/dev/nvme0n1"))
      (keyboard-layout keyboard-layout)))
  (swap-devices
    (list (swap-space (target (uuid "2d8ba0a3-f783-444c-ad50-748f92330f48")))))
  (file-systems
    (append (list
             (file-system
               (mount-point "/")
               (device
                 (uuid "e6c71454-3221-4a38-96ca-855a096f6a05"
                       'ext4))
               (type "ext4"))
             (file-system
               (mount-point "/data")
               (device (uuid "3c31e353-931c-485a-b362-8ee26c5ff100"))
               (type "ext4"))
             (file-system
               (device "//u346902.your-storagebox.de/u346902-sub1/samba/zstd")
               (options "uid=guix-publish,gid=guix-publish,credentials=/root/samba.credentials")
               (mount-point "/var/cache/publish/zstd")
               (mount? #f)
               (type "cifs")))
            %base-file-systems))

  (packages
   (append
    (map (compose list specification->package+output)
     (list
       "bind:utils"
       "cifs-utils" "curl"
       "git"
       "htop"
       "ncdu"
       "nmap"
       "qemu"
       "rsync"
       "screen"
       "smartmontools"
       "vim" "vim-airline" "vim-guix-vim"
       "wget"
       "zstd"))
    %base-packages))

  (services
    (cons*
      (service avahi-service-type)

            (service certbot-service-type
              (certbot-configuration
               (certificates
                (list
                 (certificate-configuration
                   (deploy-hook %nginx-deploy-hook)
                   (domains '("nonguix.org"
                              "cuirass.nonguix.org"
                              "substitutes.nonguix.org")))))
                   (email "jonathan.brielmaier@web.de")
                   (webroot "/srv/http")))

            (service cuirass-service-type
              (cuirass-configuration
                (remote-server
                  (cuirass-remote-server-configuration
                    (private-key "/etc/guix/signing-key.sec")
                    (public-key "/etc/guix/signing-key.pub")
                    (publish? #f)
                    (trigger-url "http://localhost:8080")))
                (specifications %cuirass-specs)))

            (service guix-publish-service-type
              (guix-publish-configuration
                ;; Requires manual: sudo mkdir /var/cache/publish
                ;; sudo chown -R guix-publish:guix-publish /var/cache/publish
                (cache "/var/cache/publish")
                (compression '(("zstd" 19)))
                (port 8080)))

            ;; TODO: base on http://issues.guix.gnu.org/48975
            (service iptables-service-type
              (iptables-configuration
                (ipv4-rules (plain-file "iptables.rules" "*filter
-A INPUT -p tcp --dport 5522 ! -s 127.0.0.1 -j REJECT
-A INPUT -p tcp --dport 5555:5558 ! -s 127.0.0.1 -j REJECT
-A INPUT -p tcp --dport 8080:8081 ! -s 127.0.0.1 -j REJECT
COMMIT
"))))

            (service nginx-service-type
              (nginx-configuration
               (upstream-blocks
                (list
                 (nginx-upstream-configuration
                   (name "guix-cuirass")
                   (servers (list "localhost:8081")))
                 (nginx-upstream-configuration
                   (name "guix-publish")
                   (servers (list "localhost:8080")))))
               (server-blocks
                (list
                 (nginx-server-configuration
                   (server-name '("nonguix.org"))
                   (raw-content '("return 301 $scheme://gitlab.com/nonguix/nonguix;"))
                   (listen '("443 ssl" "[::]:443 ssl"))
                   (ssl-certificate "/etc/letsencrypt/live/nonguix.org/fullchain.pem")
                   (ssl-certificate-key "/etc/letsencrypt/live/nonguix.org/privkey.pem"))
                 (nginx-server-configuration
                   (server-name '("cuirass.nonguix.org"))
                   (listen '("443 ssl" "[::]:443 ssl"))
                   (locations
                    (list
                     (nginx-location-configuration
                       (uri "~ ^/admin")
                       (body
                        (list "if ($ssl_client_verify != SUCCESS) { return 403; } proxy_pass http://guix-cuirass;")))
                     (nginx-location-configuration
                       (uri "/")
                       (body '("proxy_pass http://guix-cuirass;")))))
                   (ssl-certificate "/etc/letsencrypt/live/nonguix.org/fullchain.pem")
                   (ssl-certificate-key "/etc/letsencrypt/live/nonguix.org/privkey.pem"))
                 (nginx-server-configuration
                   (server-name '("substitutes.nonguix.org"))
                   (listen '("443 ssl" "[::]:443 ssl"))
                   (raw-content '("rewrite ^//(.*)$ /$1 redirect;"))
                   (index (list "substitutes.index.html"))
                   (locations
                    (list
                     (nginx-location-configuration
                       (uri "/signing-key.pub")
                       (body '("proxy_pass http://guix-publish;")))
                     (nginx-location-configuration
                       (uri "/file/")
                       (body '("proxy_pass http://guix-publish;")))
                     (nginx-location-configuration
                       (uri "/log/")
                       (body '("proxy_pass http://guix-publish;")))
                     (nginx-location-configuration
                       (uri "/nix-cache-info")
                       (body (list
                         "proxy_pass http://guix-publish;"
                         "proxy_hide_header Set-Cookie;")))
                     (nginx-location-configuration
                       (uri "/nar/")
                       (body (list
                         "proxy_pass http://guix-publish;"
                         "client_body_buffer_size 256k;"
                         ;; Nars are already compressed. -> no perf change
                         "gzip off;"
                         "proxy_pass_header Cache-Control;")))
                     (nginx-location-configuration
                       (uri "~ \\.narinfo$")
                       (body
                        (list
                          "proxy_pass http://guix-publish;"
                          "client_body_buffer_size 128k;"
                          "proxy_connect_timeout 2s;"
                          "proxy_read_timeout 2s;"
                          "proxy_send_timeout 2s;"
                          "proxy_pass_header Cache-Control;"
                          "proxy_ignore_client_abort on;")))))
                   (ssl-certificate "/etc/letsencrypt/live/nonguix.org/fullchain.pem")
                   (ssl-certificate-key "/etc/letsencrypt/live/nonguix.org/privkey.pem"))))))

      (service ntp-service-type)

            (service openssh-service-type
              (openssh-configuration
                (authorized-keys
                 `(("jonathan" ,(local-file "keys/ssh/jonathan.pub"))
                   ("podiki" ,(local-file "keys/ssh/podiki.pub"))))
                (password-authentication? #f)
                (port-number 2123)))

      (service postgresql-service-type
        (postgresql-configuration
          (postgresql postgresql-13)))
      (service postgresql-role-service-type)

      (service static-networking-service-type
        (list (static-networking
          (addresses
            (list (network-address
                    (device "enp9s0")
                    (value "144.76.7.123/27"))
                  (network-address
                    (device "enp9s0")
                    (ipv6? #t)
                    (value "2a01:4f8:190:8242::1/64"))))
          (routes
            (list (network-route
                    (destination "default")
                    (device "enp9s0")
                    (gateway "144.76.7.97"))
                  (network-route
                    (destination "default")
                    (device "enp9s0")
                    (ipv6? #t)
                    (gateway "fe80::1"))))
          (name-servers '("185.12.64.1" "185.12.64.2"
                          "2001:4860:4860::8888"
                          "2001:4860:4860::8844")))))

      (simple-service 'cron-jobs
                      mcron-service-type
                      (list garbage-collector-job
                            mount-all-job))

            ;; Browse through bash history via PageUp/PageDown
            (simple-service 'inputrc etc-service-type
             `(("inputrc" ,(plain-file "inputrc"
                             (string-append
                               "\"\\e[5~\": history-search-backward\n"
                               "\"\\e[6~\": history-search-forward\n")))))

      (simple-service 'worker-vm
       shepherd-root-service-type
       (list
        (shepherd-service
         (requirement '(file-systems networking))
         (provision '(worker-vm))
         (documentation "Runs the qemu VM specified in worker-vm.scm")
         (start
          (with-service-gexp-modules
           #~(begin
               (lambda _
                 (let ((cmd (list #$(file-append qemu "/bin/qemu-system-x86_64")
                                  "-enable-kvm"
                                  "-nographic"
                                  "-m"      "50G"
                                  "-smp"    "8"
                                  "-device" "e1000,netdev=net0"
                                  "-netdev" "user,id=net0,hostfwd=tcp::5522-:22,hostfwd=tcp::5558-:5558"
                                  "-drive"  "file=/data/worker-vm.qcow2,if=virtio,cache=writeback,werror=report"
                                  "-serial" "mon:stdio")))
                   (fork+exec-command
                    cmd
                    #:log-file "/var/log/worker-vm.log")))))))))

      (modify-services %base-services
        (guix-service-type config =>
          (guix-configuration
            (inherit config)
            (authorized-keys (append
                               %default-authorized-guix-keys
                               (list (local-file "keys/guix/worker-vm.pub"))))))))))
